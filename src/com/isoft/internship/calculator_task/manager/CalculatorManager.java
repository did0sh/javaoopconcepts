package com.isoft.internship.calculator_task.manager;

import java.util.Scanner;

import com.isoft.internship.calculator_task.constants.GlobalConstants;
import com.isoft.internship.calculator_task.entity.Calculator;

/**
 * Manager for all the calculations
 * 
 * @author Deyan Georgiev
 */
public class CalculatorManager {

	public void run() {
		Calculator calculator = Calculator.getInstance();
		calculator.setOperations();

		Scanner scan = new Scanner(System.in);
		System.out.println(GlobalConstants.ENTER_MESSAGE);

		// validating the first operand
		double firstNum = validateFirstNum(scan);
		// validating the second operand
		double secondNum = validateSecondNum(scan);
		// validating the operator and calculating the result
		validateOperatorSign(scan, calculator, firstNum, secondNum);

		scan.close();
	}

	private static double validateFirstNum(Scanner scan) {
		double firstNum;
		System.out.print(GlobalConstants.ENTER_FIRST_NUMBER_MESSAGE);
		while (true) {
			try {
				firstNum = Double.parseDouble(scan.nextLine());
				break;
			} catch (Exception e) {
				System.out.println(GlobalConstants.ENTER_A_VALID_NUMBER_MESSAGE);
				System.out.print(GlobalConstants.ENTER_FIRST_NUMBER_MESSAGE);
			}
		}

		return firstNum;
	}

	private static double validateSecondNum(Scanner scan) {
		double secondNum;
		System.out.print(GlobalConstants.ENTER_SECOND_NUMBER_MESSAGE);
		while (true) {
			try {
				secondNum = Double.parseDouble(scan.nextLine());
				break;
			} catch (Exception e) {
				System.out.println(GlobalConstants.ENTER_A_VALID_NUMBER_MESSAGE);
				System.out.print(GlobalConstants.ENTER_SECOND_NUMBER_MESSAGE);
			}
		}

		return secondNum;
	}

	private static char validateOperatorSign(Scanner scan, Calculator calculator, double firstNum, double secondNum) {
		char operator = 0;
		System.out.print(GlobalConstants.ENTER_OPERATOR_SIGN_MESSAGE);
		boolean isNotCalculated = true;
		String input;
		while (isNotCalculated) {
			try {
				input = scan.next();
				if (input.length() > 1) {
					System.out.println(GlobalConstants.ENTER_A_VALID_OPERATOR_SIGN_MESSAGE);
					System.out.print(GlobalConstants.ENTER_OPERATOR_SIGN_MESSAGE);
					continue;
				}
				operator = input.charAt(0);
			} catch (Exception e) {
				System.out.println(GlobalConstants.ENTER_A_VALID_OPERATOR_SIGN_MESSAGE);
				System.out.print(GlobalConstants.ENTER_OPERATOR_SIGN_MESSAGE);
			}

			switch (operator) {
			case '+':
			case '-':
			case '*':
			case '/':
			case '^':
				calculator.setAttributes(operator, firstNum, secondNum);
				calculator.makeCalculation();
				isNotCalculated = false;
				break;
			default:
				System.out.println(GlobalConstants.ENTER_A_VALID_OPERATOR_SIGN_MESSAGE);
				System.out.print(GlobalConstants.ENTER_OPERATOR_SIGN_MESSAGE);
				break;
			}
		}
		return operator;
	}
}
