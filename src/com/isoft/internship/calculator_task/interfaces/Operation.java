package com.isoft.internship.calculator_task.interfaces;

/**
 * Interface for all the possible operations
 * @author Deyan Georgiev
 */
public interface Operation {
	
	/**
	 * @param firstNum
	 * @param secondNum
	 * @return final result depending on the operation
	 */
	double evaluateExpression(double firstNum, double secondNum);
}
