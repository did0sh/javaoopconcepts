package com.isoft.internship.calculator_task.constants;

/**
 * Class defining the global constants
 * @author Deyan Georgiev
 */
public class GlobalConstants {

	private GlobalConstants() {}
	
	public static final String ENTER_MESSAGE = "Please type in two numbers and the operator sign:";
	public static final String ENTER_FIRST_NUMBER_MESSAGE = "Enter first number: ";
	public static final String ENTER_SECOND_NUMBER_MESSAGE = "Enter second number: ";
	public static final String ENTER_A_VALID_NUMBER_MESSAGE = "Please enter a valid number!";
	public static final String ENTER_OPERATOR_SIGN_MESSAGE = "Enter operator sign: ";
	public static final String ENTER_A_VALID_OPERATOR_SIGN_MESSAGE = "Please enter a valid operator sign! (+, -, *, /, ^)";
	public static final String FINAL_RESULT_MESSAGE = "Final result: ";
	
}
