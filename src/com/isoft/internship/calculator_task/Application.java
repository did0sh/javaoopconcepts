package com.isoft.internship.calculator_task;

import com.isoft.internship.calculator_task.manager.CalculatorManager;

/**
 * Main class of the program
 * @author Deyan Georgiev
 */
public class Application {
	public static void main(String[] args) {
		CalculatorManager manager = new CalculatorManager();
		manager.run();
	}
}
