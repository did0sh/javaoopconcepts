package com.isoft.internship.calculator_task.entity.operations;

import com.isoft.internship.calculator_task.interfaces.Operation;

/**
 * Class for division
 * @author Deyan Georgiev
 */
public class Divison implements Operation {

	@Override
	public double evaluateExpression(double firstNum, double secondNum) {
		return firstNum / secondNum;
	}

}
