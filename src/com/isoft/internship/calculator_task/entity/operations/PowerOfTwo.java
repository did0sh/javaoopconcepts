package com.isoft.internship.calculator_task.entity.operations;

import com.isoft.internship.calculator_task.interfaces.Operation;

/**
 * Class for powering two values
 * @author Deyan Georgiev
 */
public class PowerOfTwo implements Operation {

	@Override
	public double evaluateExpression(double firstNum, double secondNum) {
		return Math.pow(firstNum, secondNum);
	}

}
