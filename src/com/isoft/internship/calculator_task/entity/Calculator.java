package com.isoft.internship.calculator_task.entity;

import java.util.LinkedHashMap;
import java.util.Map;

import com.isoft.internship.calculator_task.constants.GlobalConstants;
import com.isoft.internship.calculator_task.entity.operations.Addition;
import com.isoft.internship.calculator_task.entity.operations.Divison;
import com.isoft.internship.calculator_task.entity.operations.Multiplication;
import com.isoft.internship.calculator_task.entity.operations.PowerOfTwo;
import com.isoft.internship.calculator_task.entity.operations.Subtraction;
import com.isoft.internship.calculator_task.interfaces.Operation;

/**
 * Class calculator, which can be instantiated only once
 * @author Deyan Georgiev
 */
public class Calculator {
	private static final Calculator CALCULATOR_INSTANCE = new Calculator();
	
	private char operator;
	private double firstOperand;
	private double secondOperand;
	
	private Map<Character, Operation> allOperations = new LinkedHashMap<>();
	
	/**
	 * private constructor to avoid client applications to use constructor
	 */
	private Calculator() {}
	
	public static Calculator getInstance() {
		return CALCULATOR_INSTANCE;
	}
	
	/**
	 * @param operator
	 * @param firstOperand
	 * @param secondOperand
	 */
	public void setAttributes(char operator, double firstOperand, double secondOperand) {
		this.operator = operator;
		this.firstOperand = firstOperand;
		this.secondOperand = secondOperand;
	}
	
	/**
	 * putting all operations in a map
	 */
	public void setOperations() {
		this.allOperations.put('+', new Addition());
		this.allOperations.put('-', new Subtraction());
		this.allOperations.put('*', new Multiplication());
		this.allOperations.put('/', new Divison());
		this.allOperations.put('^', new PowerOfTwo());
	}
	
	/**
	 * making the calculation based on the operation
	 */
	public void makeCalculation() {
		Operation operationMapValue = null;
        if (allOperations.containsKey(operator)) {
            operationMapValue = allOperations.get(operator);
            System.out.println(GlobalConstants.FINAL_RESULT_MESSAGE + operationMapValue.evaluateExpression(firstOperand, secondOperand));
        } else {
            System.out.println(GlobalConstants.ENTER_A_VALID_OPERATOR_SIGN_MESSAGE);
        }
	}
}
