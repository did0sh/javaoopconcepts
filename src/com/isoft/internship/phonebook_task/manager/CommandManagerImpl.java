package com.isoft.internship.phonebook_task.manager;

import java.util.Map;
import java.util.Scanner;

import com.isoft.internship.phonebook_task.constants.GlobalConstants;
import com.isoft.internship.phonebook_task.entity.PhoneBook;
import com.isoft.internship.phonebook_task.entity.User;
import com.isoft.internship.phonebook_task.interfaces.CommandManager;

/**
 * Class manager for the user input and output
 * 
 * @author Deyan Georgiev
 */
public class CommandManagerImpl implements CommandManager {

	private final PhoneBook phoneBook = PhoneBook.getInstance();

	@Override
	public void run() {
		Scanner scan = new Scanner(System.in);
		System.out.println(GlobalConstants.WELCOME_MESSAGE);
		intializeMenu();
		int chosenNumber = getChosenNumber(scan);

		while (true) {
			switch (chosenNumber) {
			case 1:
				chosenNumber = saveCommand(scan, phoneBook);
				break;
			case 2:
				chosenNumber = searchCommand(scan, phoneBook);
				break;
			case 3:
				chosenNumber = deleteCommand(scan, phoneBook);
				break;
			case 4:
				chosenNumber = listCommand(scan, phoneBook);
				break;
			case 5:
				return;
			default:
				System.out.println(GlobalConstants.ISSUE_MESSAGE);
				break;
			}
		}
	}

	private static void intializeMenu() {
		System.out.println(GlobalConstants.NUMBERS_ONE_TO_FIVE_MESSAGE);
		System.out.println(GlobalConstants.CASE_ONE_MESSAGE);
		System.out.println(GlobalConstants.CASE_TWO_MESSAGE);
		System.out.println(GlobalConstants.CASE_THREE_MESSAGE);
		System.out.println(GlobalConstants.CASE_FOUR_MESSAGE);
		System.out.println(GlobalConstants.CASE_FIVE_MESSAGE);
		System.out.print(GlobalConstants.CHOICE_MESSAGE);

	}

	private static int getChosenNumber(Scanner scan) {
		int chosenNumber;
		while (true) {
			try {
				chosenNumber = Integer.parseInt(scan.nextLine());
				if (chosenNumber < 1 || chosenNumber > 5) {
					System.out.println(GlobalConstants.ENTER_NUMBER_ONE_TO_FIVE_MESSAGE);
					System.out.print(GlobalConstants.CHOICE_MESSAGE);
					continue;
				}
				break;
			} catch (Exception e) {
				System.out.println(GlobalConstants.ENTER_A_VALID_NUMBER_MESSAGE);
				System.out.print(GlobalConstants.CHOICE_MESSAGE);
			}
		}

		return chosenNumber;

	}

	/**
	 * @param scan
	 * @param phoneBook
	 * @return the next chosen number
	 */
	private static int saveCommand(Scanner scan, PhoneBook phoneBook) {
		int chosenNumber = 0;
		boolean addMoreNumbers = true;
		while (addMoreNumbers) {
			System.out.print(GlobalConstants.ENTER_PHONE_NUMBER_ADD_MESSAGE);
			String phoneNumber = scan.nextLine();

			System.out.print(GlobalConstants.ENTER_PERSON_NAME_MESSAGE);
			String name = scan.nextLine();

			System.out.print(GlobalConstants.ENTER_PERSON_ADDRESS_MESSAGE);
			String address = scan.nextLine();

			User user = new User(name, address);
			phoneBook.setAttributes(phoneNumber, user);
			phoneBook.saveInformation();

			boolean isNotAnswered = true;
			while (isNotAnswered) {
				System.out.print(GlobalConstants.QUESTION_ADD_NUMBER);
				String answer = scan.nextLine();

				if (answer.equalsIgnoreCase(GlobalConstants.ANSWER_NO)) {
					isNotAnswered = false;
					addMoreNumbers = false;
					intializeMenu();
					chosenNumber = getChosenNumber(scan);
				} else if (answer.equalsIgnoreCase(GlobalConstants.ANSWER_YES)) {
					isNotAnswered = false;
				} else {
					System.out.println(GlobalConstants.ENTER_A_VALID_ANSWER_MESSAGE);
				}
			}
		}
		return chosenNumber;
	}

	/**
	 * @param scan
	 * @param phoneBook
	 * @return the next chosen number
	 */
	private static int searchCommand(Scanner scan, PhoneBook phoneBook) {
		int chosenNumber = 0;
		boolean searchMoreNumbers = true;
		while (searchMoreNumbers) {
			System.out.print(GlobalConstants.ENTER_PHONE_NUMBER_SEARCH_MESSAGE);
			String phoneNumber = scan.nextLine();

			phoneBook.searchInformation(phoneNumber);

			boolean isNotAnswered = true;
			while (isNotAnswered) {
				System.out.print(GlobalConstants.QUESTION_SEARCH_NUMBER);
				String answer = scan.nextLine();

				if (answer.equalsIgnoreCase(GlobalConstants.ANSWER_NO)) {
					isNotAnswered = false;
					searchMoreNumbers = false;
					intializeMenu();
					chosenNumber = getChosenNumber(scan);
				} else if (answer.equalsIgnoreCase(GlobalConstants.ANSWER_YES)) {
					isNotAnswered = false;
				} else {
					System.out.println(GlobalConstants.ENTER_A_VALID_ANSWER_MESSAGE);
				}
			}
		}

		return chosenNumber;
	}

	/**
	 * @param scan
	 * @param phoneBook
	 * @return the next chosen number
	 */
	private int deleteCommand(Scanner scan, PhoneBook phoneBook) {
		int chosenNumber = 0;
		boolean deleteMoreNumbers = true;
		while (deleteMoreNumbers) {
			System.out.print(GlobalConstants.ENTER_PHONE_NUMBER_DELETE_MESSAGE);
			String phoneNumber = scan.nextLine();

			phoneBook.deleteInformation(phoneNumber);

			boolean isNotAnswered = true;
			while (isNotAnswered) {
				System.out.print(GlobalConstants.QUESTION_DELETE_NUMBER);
				String answer = scan.nextLine();

				if (answer.equalsIgnoreCase(GlobalConstants.ANSWER_NO)) {
					isNotAnswered = false;
					deleteMoreNumbers = false;
					intializeMenu();
					chosenNumber = getChosenNumber(scan);
				} else if (answer.equalsIgnoreCase(GlobalConstants.ANSWER_YES)) {
					isNotAnswered = false;
				} else {
					System.out.println(GlobalConstants.ENTER_A_VALID_ANSWER_MESSAGE);
				}
			}
		}

		return chosenNumber;
	}

	/**
	 * @param scan
	 * @param phoneBook
	 * @return the next chosen number
	 */
	private int listCommand(Scanner scan, PhoneBook phoneBook) {
		Map<String, User> allInformation = phoneBook.getContainer().getAllPhoneBookInformation();
		if (!allInformation.isEmpty()) {
			allInformation.forEach((key, value) -> System.out.println(key + " -> " + value));
		} else {
			System.out.println(GlobalConstants.EMPTY_PHONE_BOOK_MESSAGE);
		}

		intializeMenu();
		return getChosenNumber(scan);
	}
}
