package com.isoft.internship.phonebook_task.interfaces;

import java.util.Map;

import com.isoft.internship.phonebook_task.container.PhoneBookContainerImpl;
import com.isoft.internship.phonebook_task.entity.User;

/**
 * Interface for adding all phone numbers
 * 
 * @author Deyan Georgiev
 * @see PhoneBookContainerImpl
 */
public interface PhoneBookContainer {

	/**
	 * Putting every unique number with his referent user name
	 * 
	 * @param number
	 * @param user
	 */
	void addNumber(String number, User user);

	/**
	 * Searching in all users by their phone number
	 * 
	 * @param number
	 */
	void searchByNumber(String number);

	/**
	 * Deleting the user who has this number
	 * 
	 * @param number
	 */
	void deleteByNumber(String number);

	/**
	 * Returns the all information from the phone book.
	 */
	Map<String, User> getAllPhoneBookInformation();
}
