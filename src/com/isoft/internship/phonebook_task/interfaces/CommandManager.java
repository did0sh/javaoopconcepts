package com.isoft.internship.phonebook_task.interfaces;

import com.isoft.internship.phonebook_task.manager.CommandManagerImpl;

/**
 * Interface for starting the program
 * 
 * @author Deyan Georgiev
 * @see CommandManagerImpl
 */
public interface CommandManager {

	/**
	 * Method for running the application
	 */
	void run();
}
