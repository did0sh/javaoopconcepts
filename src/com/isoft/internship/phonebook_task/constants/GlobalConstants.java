package com.isoft.internship.phonebook_task.constants;

/**
 * Class for replacing the magic strings with constant variables
 * 
 * @author Deyan Georgiev
 */
public class GlobalConstants {

	/**
	 * private constructor to prevent instantiating
	 */
	private GlobalConstants() {
	}

	public static final String WELCOME_MESSAGE = "Welcome to the phone book app!";
	public static final String ISSUE_MESSAGE = "We are having an issue. Please try again later!";

	public static final String NUMBERS_ONE_TO_FIVE_MESSAGE = "Please choose a number [1-5]: ";
	public static final String CASE_ONE_MESSAGE = "1. Enter phone and name to save it";
	public static final String CASE_TWO_MESSAGE = "2. Search a user by phone number";
	public static final String CASE_THREE_MESSAGE = "3. Delete a user by phone number";
	public static final String CASE_FOUR_MESSAGE = "4. List all phone numbers";
	public static final String CASE_FIVE_MESSAGE = "5. Exit";
	public static final String CHOICE_MESSAGE = "Choice: ";

	public static final String ENTER_NUMBER_ONE_TO_FIVE_MESSAGE = "Please enter a number between 1 and 5!";
	public static final String ENTER_A_VALID_NUMBER_MESSAGE = "Please enter a valid number!";

	public static final String ENTER_PHONE_NUMBER_ADD_MESSAGE = "Enter phone number to add: ";
	public static final String ENTER_PERSON_NAME_MESSAGE = "Enter the person`s name: ";
	public static final String ENTER_PERSON_ADDRESS_MESSAGE = "Enter the person`s address: ";

	public static final String QUESTION_ADD_NUMBER = "Do you want to add another number? (Yes/No): ";
	public static final String ANSWER_NO = "No";
	public static final String ANSWER_YES = "Yes";
	public static final String ENTER_A_VALID_ANSWER_MESSAGE = "Please enter a valid answer!";

	public static final String ENTER_PHONE_NUMBER_SEARCH_MESSAGE = "Enter phone number for searching: ";
	public static final String QUESTION_SEARCH_NUMBER = "Do you want to search for another number? (Yes/No): ";

	public static final String ENTER_PHONE_NUMBER_DELETE_MESSAGE = "Enter phone number for deleting: ";
	public static final String QUESTION_DELETE_NUMBER = "Do you want to delete another number? (Yes/No): ";

	public static final String EMPTY_PHONE_BOOK_MESSAGE = "The phone book is empty!";

	public static final String BULGARIAN_PHONE_NUMBER_REGEX = "^08[789]\\d{7}$";
	public static final String SUCCESSFULLY_ADDED_MESSAGE = "Successfully added!";
	public static final String USER_EXISTS_MESSAGE = "There is already a user with that phone number!";
	public static final String ENTER_A_VALID_BULGARIAN_PHONE_NUMBER_MESSAGE = "Please enter a valid bulgarian phone number! (E.g.: 0888******)";

	public static final String USER_DOESNT_EXIST_MESSAGE = "User doesn`t exist!";
	public static final String SUCCESSFULLY_DELETED_MESSAGE = "Sucessfully deleted!";
}
