package com.isoft.internship.phonebook_task.entity;

import com.isoft.internship.phonebook_task.container.PhoneBookContainerImpl;
import com.isoft.internship.phonebook_task.interfaces.PhoneBookContainer;

/**
 * Singleton class containing all the information about users and their phone
 * numbers
 * 
 * @author Deyan Georgiev
 */
public class PhoneBook {

	private static final PhoneBook PHONEBOOK_INSTANCE = new PhoneBook();

	private String phoneNumber;
	private User user;
	private final PhoneBookContainer container = new PhoneBookContainerImpl();

	/**
	 * private constructor to avoid client applications to use constructor
	 */
	private PhoneBook() {
	}

	public static PhoneBook getInstance() {
		return PHONEBOOK_INSTANCE;
	}

	/**
	 * @param phoneNumber
	 * @param user
	 */
	public void setAttributes(String phoneNumber, User user) {
		this.phoneNumber = phoneNumber;
		this.user = user;
	}

	/**
	 * save all info in the container
	 */
	public void saveInformation() {
		container.addNumber(phoneNumber, user);
	}

	/**
	 * search all info in the container
	 */
	public void searchInformation(String numberToSearch) {
		container.searchByNumber(numberToSearch);
	}

	/**
	 * search all info in the container
	 */
	public void deleteInformation(String numberToDelete) {
		container.deleteByNumber(numberToDelete);
	}

	public PhoneBookContainer getContainer() {
		return container;
	}
}
