package com.isoft.internship.phonebook_task.entity;

/**
 * Class user containing the information about a given person
 * 
 * @author Deyan Georgiev
 */
public class User {
	private String name;
	private String livingAddress;

	/**
	 * @param name
	 * @param livingAddress
	 */
	public User(String name, String livingAddress) {
		this.name = name;
		this.livingAddress = livingAddress;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the livingAddress
	 */
	public String getLivingAddress() {
		return livingAddress;
	}

	@Override
	public String toString() {
		return "User: " + this.getName() + ", " + "Address: " + this.getLivingAddress();
	}

}
