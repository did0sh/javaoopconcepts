package com.isoft.internship.phonebook_task.container;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.isoft.internship.phonebook_task.constants.GlobalConstants;
import com.isoft.internship.phonebook_task.entity.User;
import com.isoft.internship.phonebook_task.interfaces.PhoneBookContainer;

/**
 * Class containing all the numbers and names
 * 
 * @author Deyan Georgiev
 */
public class PhoneBookContainerImpl implements PhoneBookContainer {

	/**
	 * Map with unique phone numbers and their represented users
	 */
	private final Map<String, User> phoneBookMap = new LinkedHashMap<>();

	private final Pattern pattern = Pattern.compile(GlobalConstants.BULGARIAN_PHONE_NUMBER_REGEX);

	/**
	 * @return appropriate message for each case
	 */
	@Override
	public void addNumber(String number, User user) {
		Matcher matcher = pattern.matcher(number);
		if (matcher.find()) {
			if (!phoneBookMap.containsKey(number)) {
				phoneBookMap.put(number, user);
				System.out.println(GlobalConstants.SUCCESSFULLY_ADDED_MESSAGE);
			} else {
				System.out.println(GlobalConstants.USER_EXISTS_MESSAGE);
			}
		} else {
			System.out.println(GlobalConstants.ENTER_A_VALID_BULGARIAN_PHONE_NUMBER_MESSAGE);
		}
	}

	/**
	 * @return message if there is no such user or user.toString();
	 * @see User
	 */
	@Override
	public void searchByNumber(String number) {
		Matcher matcher = pattern.matcher(number);
		if (matcher.find()) {
			User user = this.getAllPhoneBookInformation().get(number);
			if (user == null) {
				System.out.println(GlobalConstants.USER_DOESNT_EXIST_MESSAGE);
			} else {
				System.out.println(user);
			}
		} else {
			System.out.println(GlobalConstants.ENTER_A_VALID_BULGARIAN_PHONE_NUMBER_MESSAGE);
		}
	}

	/**
	 * @return message if the user is deleted or not
	 */
	@Override
	public void deleteByNumber(String number) {
		Matcher matcher = pattern.matcher(number);
		if (matcher.find()) {
			User removedUser = this.phoneBookMap.remove(number);
			if (removedUser == null) {
				System.out.println(GlobalConstants.USER_DOESNT_EXIST_MESSAGE);
			} else {
				System.out.println(GlobalConstants.SUCCESSFULLY_DELETED_MESSAGE);
			}
		} else {
			System.out.println(GlobalConstants.ENTER_A_VALID_BULGARIAN_PHONE_NUMBER_MESSAGE);
		}
	}

	/**
	 * @return collection containing all numbers and names
	 */
	@Override
	public Map<String, User> getAllPhoneBookInformation() {
		return Collections.unmodifiableMap(this.phoneBookMap);
	}

}
