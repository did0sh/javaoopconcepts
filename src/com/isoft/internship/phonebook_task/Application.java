package com.isoft.internship.phonebook_task;

import com.isoft.internship.phonebook_task.interfaces.CommandManager;
import com.isoft.internship.phonebook_task.manager.CommandManagerImpl;

/**
 * Main class of the program
 * 
 * @author Deyan Georgiev
 * @see CommandManagerImpl
 */
public class Application {
	public static void main(String[] args) {
		CommandManager manager = new CommandManagerImpl();
		manager.run();
	}
}
