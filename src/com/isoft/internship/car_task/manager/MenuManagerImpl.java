package com.isoft.internship.car_task.manager;

import com.isoft.internship.car_task.constants.GlobalConstants;
import com.isoft.internship.car_task.entity.BaseCar;
import com.isoft.internship.car_task.enums.CarBrand;
import com.isoft.internship.car_task.factory.CarFactory;
import com.isoft.internship.car_task.factory.CarFactoryImpl;
import com.isoft.internship.car_task.io.ConsoleInputReader;
import com.isoft.internship.car_task.io.ConsoleOutputWriter;

/**
 * Manager for the input and output
 * 
 * @author Deyan Georgiev
 */
public class MenuManagerImpl implements MenuManager {

	private ConsoleInputReader reader;
	private ConsoleOutputWriter writer;
	private CarFactory carCreator;

	public MenuManagerImpl() {
		this.reader = new ConsoleInputReader();
		this.writer = new ConsoleOutputWriter();
		this.carCreator = new CarFactoryImpl();
	}

	@Override
	public void run() {
		CarBrand[] brands = CarBrand.values();
		for (CarBrand carBrand : brands) {
			carCreator.createCar(carBrand);
		}

		writer.writeLine(GlobalConstants.WELCOME_MESSAGE);
		writer.writeLine(GlobalConstants.SELECT_MESSAGE);

		int carsSize = carCreator.getAllCars().size();
		int count = 0;
		for (int i = 0; i < carsSize; i++) {
			count++;
			BaseCar car = carCreator.getAllCars().get(i);
			writer.writeLine(String.format(GlobalConstants.CARS_TO_SELECT_FORMAT, count, car.getClass().getSimpleName()));
		}

		writer.writeLine(GlobalConstants.EXIT_MESSAGE);

		String input;
		int carIndex;
		
		while (true) {
			writer.write(GlobalConstants.ENTER_A_NUMBER_MESSAGE);
			input = reader.readLine();
			switch (input) {
			case "1":
			case "2":
			case "3":
			case "4":
			case "5":
				carIndex = Integer.parseInt(input) - 1;
				BaseCar currentCar = carCreator.getAllCars().get(carIndex);
				writer.writeLine(currentCar.toString());
				break;
			case "6":
				return;
			default:
				writer.writeLine(GlobalConstants.ENTER_VALID_NUMBER_MESSAGE);
				break;
			}
		}

	}

}
