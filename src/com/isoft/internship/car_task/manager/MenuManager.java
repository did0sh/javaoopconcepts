package com.isoft.internship.car_task.manager;

/**
 * Interface for the implementation of the menu manager
 * @author Deyan Georgiev
 */
public interface MenuManager {
	void run();
}
