package com.isoft.internship.car_task;

import com.isoft.internship.car_task.manager.MenuManager;
import com.isoft.internship.car_task.manager.MenuManagerImpl;

/**
 * @author Deyan Georgiev
 */
public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MenuManager manager = new MenuManagerImpl();
		manager.run();
	}

}
