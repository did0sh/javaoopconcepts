package com.isoft.internship.car_task.enums;

/**
 * Enum for the type of car engine
 * @author Deyan Georgiev
 */
public enum EngineType {
	DIESEL, BENZIN, HYBRID, ELECTRIC;
}
