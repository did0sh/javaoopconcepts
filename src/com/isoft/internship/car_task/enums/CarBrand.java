package com.isoft.internship.car_task.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Enumeration for the car brands with the models as values
 * @author Deyan Georgiev
 */
public enum CarBrand {
	AUDI("A3", "A4", "A7"), 
	BMW("E46", "X5", "I8"), 
	MERCEDES("C220", "E320", "S500"), 
	VOLKSWAGEN("POLO", "GOLF", "PASSAT"), 
	TOYOTA("AURIS", "YARIS", "RAV4");
	
	private List<String> models;
	
	/**
	 * @param models
	 */
	private CarBrand(String... models) {
		this.models = Arrays.asList(models);
	}
	
	/**
	 * @return unmodifiable collection of the models
	 */
	public List<String> getModels(){
		return Collections.unmodifiableList(models);
	}
}
