package com.isoft.internship.car_task.enums;

/**
 * Enumeration for the transmission type of the car
 * @author Deyan Georgiev
 */
public enum TransmissionType {
	MANUAL, AUTOMATIC
}
