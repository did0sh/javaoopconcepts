package com.isoft.internship.car_task.enums;

/**
 * Enumeration for the condition of the car
 * @author Deyan Georgiev
 */
public enum Condition {
	NEW, USED, DAMAGED
}
