package com.isoft.internship.car_task.io;

/**
 * Class for the output
 * @author Deyan Georgiev
 */
public class ConsoleOutputWriter implements OutputWriter {

	@Override
	public void write(String output) {
		System.out.print(output);
	}

	@Override
	public void writeLine(String output) {
		System.out.println(output);
	}

}
