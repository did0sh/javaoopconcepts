package com.isoft.internship.car_task.io;

/**
 * Interface with methods for writing at the same/new line
 * @author Deyan Georgiev
 */
public interface OutputWriter {
	void write(String output);
	
	void writeLine(String output);
}
