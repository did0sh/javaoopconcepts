package com.isoft.internship.car_task.io;

import java.io.IOException;

/**
 * Interface with a method that reads a line
 * @author Deyan Georgiev
 */
public interface InputReader {
	
	String readLine() throws IOException;

}
