package com.isoft.internship.car_task.entity;

import com.isoft.internship.car_task.constants.GlobalConstants;
import com.isoft.internship.car_task.enums.CarBrand;
import com.isoft.internship.car_task.enums.Condition;
import com.isoft.internship.car_task.enums.EngineType;
import com.isoft.internship.car_task.enums.TransmissionType;

/**
 * Abstract class containing all common information about a car
 * 
 * @author Deyan Georgiev
 */
public abstract class BaseCar {
	private CarBrand brand;
	private EngineType engineType;
	private TransmissionType transmissionType;
	private Condition condition;
	private int yearOfProduction;
	private double price;

	/**
	 * @param brand
	 * @param engineType
	 * @param transmissionType
	 * @param condition
	 * @param yearOfProduction
	 * @param price
	 */
	protected BaseCar(CarBrand brand, EngineType engineType, TransmissionType transmissionType, Condition condition,
			int yearOfProduction, double price) {
		setBrand(brand);
		setEngineType(engineType);
		setTransmissionType(transmissionType);
		setCondition(condition);
		setYearOfProduction(yearOfProduction);
		setPrice(price);
	}

	/**
	 * @return the brand
	 */
	public CarBrand getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	private void setBrand(CarBrand brand) {
		this.brand = brand;
	}

	/**
	 * @return the engineType
	 */
	public EngineType getEngineType() {
		return engineType;
	}

	/**
	 * @param engineType the engineType to set
	 */
	private void setEngineType(EngineType engineType) {
		this.engineType = engineType;
	}

	/**
	 * @return the transmissionType
	 */
	public TransmissionType getTransmissionType() {
		return transmissionType;
	}

	/**
	 * @param transmissionType the transmissionType to set
	 */
	private void setTransmissionType(TransmissionType transmissionType) {
		this.transmissionType = transmissionType;
	}

	/**
	 * @return the condition
	 */
	public Condition getCondition() {
		return condition;
	}

	/**
	 * @param condition the condition to set
	 */
	private void setCondition(Condition condition) {
		this.condition = condition;
	}

	/**
	 * @return the yearOfProduction
	 */
	public int getYearOfProduction() {
		return yearOfProduction;
	}

	/**
	 * @param yearOfProduction the yearOfProduction to set
	 */
	private void setYearOfProduction(int yearOfProduction) {
		if(yearOfProduction < 1980 || yearOfProduction > 2019) {
			throw new IllegalArgumentException(GlobalConstants.ENTER_VALID_YEAR_MESSAGE);
		} else {
			this.yearOfProduction = yearOfProduction;
		}
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	private void setPrice(double price) {
		if(price < 500) {
			throw new IllegalArgumentException(GlobalConstants.ENTER_VALID_PRICE_MESSAGE);
		} else {
			this.price = price;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format(GlobalConstants.CAR_TOSTRING_FORMAT,
				this.getClass().getSimpleName(), String.join(", ", this.getBrand().getModels()), this.getEngineType(),
						this.getTransmissionType(), this.getCondition(), this.getYearOfProduction(), this.getPrice()));
		return builder.toString();
	}

}
