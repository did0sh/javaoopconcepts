package com.isoft.internship.car_task.entity;

import com.isoft.internship.car_task.enums.CarBrand;
import com.isoft.internship.car_task.enums.Condition;
import com.isoft.internship.car_task.enums.EngineType;
import com.isoft.internship.car_task.enums.TransmissionType;

/**
 * Implementation of BMW type car
 * @author Deyan Georgiev
 */
public class BMW extends BaseCar {

	/**
	 * @param brand
	 * @param engineType
	 * @param transmissionType
	 * @param condition
	 * @param yearOfProduction
	 * @param price
	 */
	public BMW(CarBrand brand, EngineType engineType, TransmissionType transmissionType, Condition condition,
			int yearOfProduction, double price) {
		super(brand, engineType, transmissionType, condition, yearOfProduction, price);
	}

}
