package com.isoft.internship.car_task.constants;

/**
 * Class with constants for the global input and output messages
 * 
 * @author Deyan Georgiev
 */
public class GlobalConstants {
	
	private GlobalConstants() {
	}
	
	public static final String WELCOME_MESSAGE = "Welcome to our car magazine!";
	public static final String SELECT_MESSAGE = "If you want to view details of the car, please select your favourite brand of the given below:";
	public static final String CARS_TO_SELECT_FORMAT = "%d. %s";
	public static final String EXIT_MESSAGE = "6. Exit";
	public static final String ENTER_VALID_NUMBER_MESSAGE = "Please enter a valid number!";
	public static final String ENTER_VALID_YEAR_MESSAGE = "Year of production is not valid!";
	public static final String ENTER_VALID_PRICE_MESSAGE = "Price is not valid!";
	public static final String CAR_TOSTRING_FORMAT = "%s - Models: %s, Engine: %s, Transmission: %s, Condition: %s, Year: %d, Price: %.2f";
	public static final String CAR_TYPE_NOT_VALID_MESSAGE = "Car type not valid";
	public static final String ENTER_A_NUMBER_MESSAGE = "Enter a number: ";
	
}
