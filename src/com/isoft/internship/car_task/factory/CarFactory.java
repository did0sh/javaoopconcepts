package com.isoft.internship.car_task.factory;

import java.util.List;

import com.isoft.internship.car_task.entity.BaseCar;
import com.isoft.internship.car_task.enums.CarBrand;

/**
 * Interface for the creation of cars
 * @author Deyan Georgiev
 */
public interface CarFactory {
	void createCar(CarBrand brand);
	
	void saveCar(BaseCar car);
	
	List<BaseCar> getAllCars();
}
