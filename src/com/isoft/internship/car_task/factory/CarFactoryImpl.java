package com.isoft.internship.car_task.factory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.isoft.internship.car_task.constants.GlobalConstants;
import com.isoft.internship.car_task.entity.Audi;
import com.isoft.internship.car_task.entity.BMW;
import com.isoft.internship.car_task.entity.BaseCar;
import com.isoft.internship.car_task.entity.Mercedes;
import com.isoft.internship.car_task.entity.Toyota;
import com.isoft.internship.car_task.entity.Volkswagen;
import com.isoft.internship.car_task.enums.CarBrand;
import com.isoft.internship.car_task.enums.Condition;
import com.isoft.internship.car_task.enums.EngineType;
import com.isoft.internship.car_task.enums.TransmissionType;

/**
 * A car creator class that creates & keeps all the cars
 * @author Deyan Georgiev
 */
public class CarFactoryImpl implements CarFactory {
	
	private List<BaseCar> allCars;
	
	/**
	 * @param allCars
	 */
	public CarFactoryImpl() {
		this.allCars = new ArrayList<>();
	}
	
	
	@Override
	public void createCar(CarBrand brand) {
		BaseCar car;
		switch(brand) {
		case AUDI: 
			car = new Audi(CarBrand.AUDI, EngineType.DIESEL, TransmissionType.AUTOMATIC, Condition.USED, 2004, 15000);
			saveCar(car);
			break;
		case BMW:
			car = new BMW(CarBrand.BMW, EngineType.BENZIN, TransmissionType.MANUAL, Condition.DAMAGED, 1999, 5000);
			saveCar(car);
			break;
		case MERCEDES:
			car = new Mercedes(CarBrand.MERCEDES, EngineType.DIESEL, TransmissionType.AUTOMATIC, Condition.NEW, 2012, 20000);
			saveCar(car);
			break;
		case TOYOTA:
			car = new Toyota(CarBrand.TOYOTA, EngineType.DIESEL, TransmissionType.AUTOMATIC, Condition.NEW, 2001, 7500);
			saveCar(car);
			break;
		case VOLKSWAGEN:
			car = new Volkswagen(CarBrand.VOLKSWAGEN, EngineType.BENZIN, TransmissionType.MANUAL, Condition.USED, 1996, 2500);
			saveCar(car);
			break;
			default:
				throw new IllegalArgumentException(GlobalConstants.CAR_TYPE_NOT_VALID_MESSAGE);
		}
	}

	@Override
	public void saveCar(BaseCar car) {
		this.allCars.add(car);
	}

	/**
	 * @return unmodifiable collection of the allCars
	 */
	@Override
	public List<BaseCar> getAllCars() {
		return Collections.unmodifiableList(this.allCars);
	}

}
