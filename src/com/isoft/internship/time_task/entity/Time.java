package com.isoft.internship.time_task.entity;

import java.time.LocalDateTime;

import com.isoft.internship.time_task.constants.GlobalConstants;

/**
 * Class that holds the current date and time
 * @author Deyan Georgiev
 */
public class Time {
	

	private String currentDateTime;
	private String eventName;
	private String eventDateTime;

	public Time() {
		setCurrentDateTime();
	}
	
	/**
	 * @param dateTime the dateTime to set
	 */
	private void setCurrentDateTime() {
		LocalDateTime dateTime = LocalDateTime.now();
		currentDateTime = GlobalConstants.DATE_TIME_FORMATTER.format(dateTime);
	}

	/**
	 * @return the currentDateTime
	 */
	public String getCurrentDateTime() {
		return currentDateTime;
	}

	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}

	/**
	 * @return the eventDateTime
	 */
	public String getEventDateTime() {
		return eventDateTime;
	}

	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	/**
	 * @param eventDateTime the eventDateTime to set
	 */
	public void setEventDateTime(String eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
}
