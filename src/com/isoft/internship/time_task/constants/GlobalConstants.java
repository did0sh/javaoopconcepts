package com.isoft.internship.time_task.constants;

import java.time.format.DateTimeFormatter;

/**
 * @author Deyan Georgiev
 */
public class GlobalConstants {
	private GlobalConstants() {
	}
	
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy (EEEE) | HH:mm:ss");
	public static final String CURRENT_DATE_TIME_MESSAGE = "The current date and time is: %s";
	public static final String EVENT_NAME_MESSAGE = "Please enter the event name: ";
	public static final String EVENT_DAY_MESSAGE = "Please enter the event day: ";
	public static final String EVENT_MONTH_MESSAGE = "Please enter the event month: ";
	public static final String EVENT_YEAR_MESSAGE = "Please enter the event year: ";
	public static final String EVENT_DATE_TIME_MESSAGE = "Please enter the time of the event in format (hour:minutes): ";
	public static final String EVENT_EXISTS_MESSAGE = "Event with that name already exists!";
	public static final String ANOTHER_EVENT_MESSAGE = "Do you want to add another event? (Yes/No): ";
	public static final String ALL_EVENTS_FORMAT = "Event: %s -> %s";
	public static final String ENTER_VALID_DAY = "Enter a valid day!";
	public static final String ENTER_VALID_MONTH = "Enter a valid month!";
	public static final String ENTER_VALID_TIME = "Enter a valid time!";
	public static final String ENTER_VALID_YEAR = "Enter a valid year!";
}
