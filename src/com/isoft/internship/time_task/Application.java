package com.isoft.internship.time_task;

import com.isoft.internship.time_task.manager.TimeManager;

/**
 * Main class for the task
 * @author Deyan Georgiev
 */
public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TimeManager manager = new TimeManager();
		manager.run();
	}

}
