package com.isoft.internship.time_task.manager;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import com.isoft.internship.time_task.constants.GlobalConstants;
import com.isoft.internship.time_task.entity.Time;

/**
 * Class that saves important events with their date and time
 * 
 * @author Deyan Georgiev
 */
public class TimeManager {

	private Map<String, Time> allEvents;

	public TimeManager() {
		allEvents = new LinkedHashMap<>();
	}

	public void run() {
		Scanner scan = new Scanner(System.in);
		Time time = new Time();

		System.out.println(String.format(GlobalConstants.CURRENT_DATE_TIME_MESSAGE, time.getCurrentDateTime()));

		boolean wantsToAddEvent = true;
		while (wantsToAddEvent) {
			time = new Time();
			System.out.print(GlobalConstants.EVENT_NAME_MESSAGE);
			String eventName = scan.nextLine();

			System.out.print(GlobalConstants.EVENT_DAY_MESSAGE);
			
			//checking if day is valid
			int day = checkIfDayIsValid(scan);
			
			System.out.print(GlobalConstants.EVENT_MONTH_MESSAGE);
			
			//check if month is valid
			int month = checkIfMonthIsValid(scan);

			System.out.print(GlobalConstants.EVENT_YEAR_MESSAGE);
			
			//check if year is valid
			int year = checkIfYearIsValid(scan);
			
			System.out.print(GlobalConstants.EVENT_DATE_TIME_MESSAGE);
			
			//check if time is valid
			String eventTime = checkIfTimeIsValid(scan);

			int hour = Integer.parseInt(eventTime.split(":")[0]);
			int minute = Integer.parseInt(eventTime.split(":")[1]);

			// creating the event
			createEvent(year, month, day, hour, minute, time, eventName);

			// check if the event exists -> if not add it
			checkIfEventExists(eventName, time, allEvents);

			// check if user wants to add another event -> if not terminate the program
			wantsToAddEvent = addAnotherEvent(scan, allEvents, wantsToAddEvent);
		}

		scan.close();
	}

	private static void checkIfEventExists(String eventName, Time time, Map<String, Time> allEvents) {
		if (!allEvents.containsKey(eventName)) {
			allEvents.put(eventName, time);
		} else {
			System.out.println(GlobalConstants.EVENT_EXISTS_MESSAGE);
		}
	}

	private static void createEvent(int year, int month, int day, int hour, int minute, Time time, String eventName) {
		LocalDateTime dateTimeEvent = LocalDateTime.of(year, month, day, hour, minute);
		String eventDateTime = GlobalConstants.DATE_TIME_FORMATTER.format(dateTimeEvent);
		time.setEventName(eventName);
		time.setEventDateTime(eventDateTime);
	}

	private static boolean addAnotherEvent(Scanner scan, Map<String, Time> allEvents, boolean wantsToAddEvent) {
		System.out.print(GlobalConstants.ANOTHER_EVENT_MESSAGE);
		String answer = scan.nextLine();
		if (answer.equalsIgnoreCase("no")) {
			wantsToAddEvent = false;
			for (Map.Entry<String, Time> keyValue : allEvents.entrySet()) {
				System.out.println(String.format(GlobalConstants.ALL_EVENTS_FORMAT, keyValue.getKey(),
						keyValue.getValue().getEventDateTime()));
			}
		}

		return wantsToAddEvent;
	}
	
	private static int checkIfDayIsValid(Scanner scan) {
		int day;
		while(true) {
			try{
				day = Integer.parseInt(scan.nextLine());
				if(day >= 1 && day <= 31) {
					break;
				} else {
					System.out.println(GlobalConstants.ENTER_VALID_DAY);
					System.out.print(GlobalConstants.EVENT_DAY_MESSAGE);
				}
			}catch (Exception e) {
				System.out.println(GlobalConstants.ENTER_VALID_DAY);
				System.out.print(GlobalConstants.EVENT_DAY_MESSAGE);
			}
		}
		
		return day;
	}
	
	private static int checkIfMonthIsValid(Scanner scan) {
		int month;
		while(true) {
			try{
				month = Integer.parseInt(scan.nextLine());
				if(month >= 1 && month <= 12) {
					break;
				} else {
					System.out.println(GlobalConstants.ENTER_VALID_MONTH);
					System.out.print(GlobalConstants.EVENT_MONTH_MESSAGE);
				}
			}catch (Exception e) {
				System.out.println(GlobalConstants.ENTER_VALID_MONTH);
				System.out.print(GlobalConstants.EVENT_MONTH_MESSAGE);
			}
		}
		
		return month;
	}
	

	private static int checkIfYearIsValid(Scanner scan) {
		int year;
		while(true) {
			try{
				year = Integer.parseInt(scan.nextLine());
				break;
			}catch (Exception e) {
				System.out.println(GlobalConstants.ENTER_VALID_YEAR);
				System.out.print(GlobalConstants.EVENT_YEAR_MESSAGE);
			}
		}
		
		return year;
	}
	
	private static String checkIfTimeIsValid(Scanner scan) {
		String eventTime;
		while(true) {
			try{
				eventTime = scan.nextLine();
				int hour = Integer.parseInt(eventTime.split(":")[0]);
				int minute = Integer.parseInt(eventTime.split(":")[1]);
				int size = eventTime.split(":").length;
				if((size == 2) && 
						(hour >= 1 && hour <= 23) && 
						(minute >= 0 && minute <= 59)) {
					break;
				} else {
					System.out.println(GlobalConstants.ENTER_VALID_TIME);
					System.out.print(GlobalConstants.EVENT_DATE_TIME_MESSAGE);
				}
			}catch (Exception e) {
				System.out.println(GlobalConstants.ENTER_VALID_TIME);
				System.out.print(GlobalConstants.EVENT_DATE_TIME_MESSAGE);
			}
		}
		
		return eventTime;
	}
}
